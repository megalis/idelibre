<?php

namespace App\Controller\ApiV2;

use App\Entity\Project;
use App\Entity\Sitting;
use App\Entity\Structure;
use App\Message\UpdatedSitting;
use App\Repository\ConvocationRepository;
use App\Repository\OtherdocRepository;
use App\Repository\ProjectRepository;
use App\Repository\SittingRepository;
use App\Security\Http400Exception;
use App\Service\ApiEntity\OtherdocApi;
use App\Service\ApiEntity\ProjectApi;
use App\Service\Otherdoc\OtherdocManager;
use App\Service\Pdf\PdfValidator;
use App\Service\Project\ProjectManager;
use App\Service\Seance\SittingManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/v2/structures/{structureId}/sittings')]
#[IsGranted('API_AUTHORIZED_STRUCTURE', subject: 'structure')]
class SittingApiController extends AbstractController
{
    public function __construct(
        private DenormalizerInterface $denormalizer,
        private MessageBusInterface $messageBus,
        private PdfValidator $pdfValidator,
        private SerializerInterface $serializer,
        private EntityManagerInterface $em
    ) {
    }


    #[Route('', name: 'get_all_sittings', methods: ['GET'])]
    public function getAll(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        Request $request,
        SittingRepository $sittingRepository
    ): JsonResponse {
        $sittings = $sittingRepository->findByStructure($structure, null, $request->query->get('status'))
            ->getQuery()->getResult();

        return $this->json($sittings, context: ['groups' => 'sitting:read']);
    }

    #[Route('/{id}', name: 'get_one_sitting', methods: ['GET'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function getById(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['id' => 'id'])] Sitting $sitting
    ): JsonResponse {
        return $this->json($sitting, context: ['groups' => ['sitting:detail', 'sitting:read']]);
    }

    #[Route('/{sittingId}/convocations', name: 'get_all_convocations_by_sitting', methods: ['GET'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function getAllConvocations(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['sittingId' => 'id'])] Sitting $sitting,
        ConvocationRepository $convocationRepository
    ): JsonResponse {
        $convocations = $convocationRepository->getConvocationsWithUserBySitting($sitting);

        return $this->json($convocations, context: ['groups' => 'convocation:read']);
    }

    #[Route('/{sittingId}/projects', name: 'get_all_projects_by_sitting', methods: ['GET'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function getAllProjects(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['sittingId' => 'id'])] Sitting $sitting,
        ProjectRepository $projectRepository
    ): JsonResponse {
        $projects = $projectRepository->getProjectsBySitting($sitting);

        return $this->json($projects, context: ['groups' => 'project:read']);
    }

    #[Route('', name: 'add_sitting', methods: ['POST'])]
    public function addSitting(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        Request $request,
        SittingManager $sittingManager
    ) {
        $context = ['groups' => ['sitting:write', 'sitting:write:post'], 'normalize_relations' => true];
        /** @var Sitting $sitting */
        $sitting = $this->denormalizer->denormalize($request->request->all(), Sitting::class, context: $context);

        if (!$request->files->get('convocationFile')) {
            throw new Http400Exception('File with key convocationFile is required');
        }

        $sittingManager->save(
            $sitting,
            $request->files->get('convocationFile'),
            $request->files->get('invitationFile') ?? null,
            $structure
        );

        return $this->json($sitting, context: ['groups' => ['sitting:detail', 'sitting:read']]);
    }

    #[Route('/{id}', name: 'update_sitting', methods: ['PUT'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function updateSitting(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['id' => 'id'])] Sitting $sitting,
        Request $request,
        SittingManager $sittingManager,
        SittingRepository $sittingRepository
    ) {
        $context = ['object_to_populate' => $sitting, 'groups' => ['sitting:write']];

        /** @var Sitting $sitting */
        $sitting = $this->denormalizer->denormalize($request->request->all(), Sitting::class, context: $context);

        $sittingManager->update(
            $sitting,
            $request->files->get('convocationFile') ?? null,
            $request->files->get('invitationFile') ?? null
        );

        $updatedSitting = $sittingRepository->find($sitting->getId());

        return $this->json($updatedSitting, context: ['groups' => ['sitting:detail', 'sitting:read']]);
    }

    #[Route('/{sittingId}/projects', name: 'add_projects_to_sitting', methods: ['POST'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function addProjectsToSitting(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['sittingId' => 'id'])] Sitting $sitting,
        Request $request,
        ProjectManager $projectManager,
        ProjectRepository $projectRepository,
        SittingManager $sittingManager
    ): JsonResponse {
        if (count($sitting->getProjects())) {
            throw new Http400Exception('Sitting already contain projects');
        }

        if ($sittingManager->isAlreadySent($sitting)) {
            throw new Http400Exception('Sitting is already sent');
        }

        $rawProjects = $request->get('projects');

        try {
            $projects = $this->serializer->deserialize($rawProjects, ProjectApi::class . '[]', 'json');
        } catch (Exception) {
            throw new Http400Exception('malformed json');
        }

        if (!$this->pdfValidator->isFilesPdf($projects)) {
            return $this->json(['success' => false, 'message' => 'Au moins un projet n\'est pas un pdf'], 400);
        }

        $projectManager->update($projects, $request->files->all(), $sitting);

        $this->messageBus->dispatch(new UpdatedSitting($sitting->getId()));

        $updated = $projectRepository->getProjectsBySitting($sitting);

        return $this->json($updated, status: 201, context: ['groups' => 'project:read']);
    }


    #[Route('/{sittingId}/otherdocs', name: 'add_otherdocs_to_sitting', methods: ['POST'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function addOtherdocsToSitting(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['sittingId' => 'id'])] Sitting $sitting,
        Request $request,
        OtherdocManager $otherdocManager,
        OtherdocRepository $otherdocRepository,
        SittingManager $sittingManager
    ): JsonResponse {
        if (count($sitting->getOtherdocs())) {
            throw new Http400Exception('Sitting already contain projects');
        }

        if ($sittingManager->isAlreadySent($sitting)) {
            throw new Http400Exception('Sitting is already sent');
        }

        $rawProjects = $request->get('otherdocs');

        try {
            $otherdocs = $this->serializer->deserialize($rawProjects, OtherdocApi::class . '[]', 'json');
        } catch (Exception) {
            throw new Http400Exception('malformed json');
        }

        if (!$this->pdfValidator->isFilesPdf($otherdocs)) {
            return $this->json(['success' => false, 'message' => 'Au moins un document n\'est pas un pdf'], 400);
        }

        $otherdocManager->update($otherdocs, $request->files->all(), $sitting);

        $this->messageBus->dispatch(new UpdatedSitting($sitting->getId()));

        $updated = $otherdocRepository->getOtherdocsBySitting($sitting);

        return $this->json($updated, status: 201, context: ['groups' => 'otherdoc:read']);
    }

    #[Route('/{sittingId}/projects/{id}', name: 'deleteProject', methods: ['DELETE'])]
    #[IsGranted('API_SAME_STRUCTURE', subject: ['structure', 'sitting'])]
    public function DeleteProject(
        #[MapEntity(mapping: ['structureId' => 'id'])] Structure $structure,
        #[MapEntity(mapping: ['sittingId' => 'id'])] Sitting $sitting,
        #[MapEntity(mapping: ['id' => 'id'])] Project $project,
        ProjectManager $projectManager,
        ProjectRepository $projectRepository,
        SittingManager $sittingManager
    ): JsonResponse {
        $projectManager->deleteProjects([$project]);
        $this->em->flush();

        $sittingManager->reorderProjects($sitting);

        $this->messageBus->dispatch(new UpdatedSitting($sitting->getId()));

        return $this->json(null, status: 204);
    }
}
