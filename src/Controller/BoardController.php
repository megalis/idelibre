<?php

namespace App\Controller;

use App\Entity\Structure;
use App\Service\Seance\SittingManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Sidebar(active: ['board-nav'])]
class BoardController extends AbstractController
{
    #[Route(path: '/board', name: 'board_index')]
    #[IsGranted('ROLE_MANAGE_SITTINGS')]
    #[Breadcrumb(title: 'Tableau de bord', routeName: 'board_index')]
    public function index(SittingManager $sittingManager, PaginatorInterface $paginator, Request $request): Response
    {
        /** @var Structure $structure */
        $structure = $this->getUser()->getStructure();
        $sittings = $paginator->paginate(
            $sittingManager->getActiveSittingDetails($this->getUser()),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.date'],
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('board/index.html.twig', [
            'sittings' => $sittings,
            'timezone' => $structure->getTimezone()->getName(),
        ]);
    }
}
