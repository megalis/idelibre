const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/app.js')
    .addEntry('vue-show-invitations', './assets/vue-app/showInvitations')
    .addEntry('vue-edit-invitations', './assets/vue-app/editInvitations')
    .addEntry('vue-manage-projects', './assets/vue-app/manageProjects.js')
    .addEntry('vue-edit-informations', './assets/vue-app/editInformations.js')
    .addEntry('enable-popover', './assets/js/enablePopover.js')
    .addEntry('enable-select2', './assets/js/enableSelect2.js')
    .addEntry('create-user', './assets/js/createUser.js')
    .addEntry('delete-user-batch', './assets/js/deleteBatch.js')
    .addEntry('select-all-users', './assets/js/selectAllUsers.js')
    .addEntry('toggle-calendar', './assets/js/toggleCalendar.js')
    .addEntry('toggle-calendar-new-sitting', './assets/js/toggleCalendarNewSitting.js')
    .addEntry('showPasswordEntropy', './assets/js/password/showPasswordEntropy.js')
    .addEntry('setPasswordUser', './assets/js/password/setPasswordUser.js')
    .addEntry('attendance', './assets/js/attendance.js')
    .addEntry('initPassword', './assets/js/password/initPassword.js')
    .addEntry('passphraseConfirmationBox', './assets/js/passphraseConfirmationBox.js')
    .addEntry('accordeonTitle', './assets/js/accordeonTitle.js')
    .addEntry('subscriptionEmails', './assets/js/subscriptionEmails.js')


        // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    //.disableSingleRuntimeChunk()
    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
